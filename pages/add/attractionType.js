import React, { useState } from 'react'
import cookie from 'cookie'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import TextField from '@material-ui/core/TextField'
import Grow from '@material-ui/core/Grow'
import AddIcon from '@material-ui/icons/Add'
import MkHeader from '../../src/MkHeader'
import { attractionType, getMe } from '../../src/api'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '90%'
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  fab: {
    margin: `${theme.spacing(1)} auto`
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },
  errorMessageContainer: {
    margin: theme.spacing(4)
  },
  errorMessage: {
    color: 'red'
  },
  successMessageContainer: {
    margin: theme.spacing(4)
  },
  successMessage: {
    color: 'green'
  }
}))

export default function AddAttractionType ({ userInfo }) {
  const classes = useStyles()

  const [name, setName] = useState('')
  const [kind, setKind] = useState('')
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)
  async function handleCreate (event) {
    const result = await attractionType.add(name, kind)

    if (result.error) {
      setSuccess(false)
      setError(result.message)
    } else {
      setSuccess(true)
      setName('')
      setKind('')
      setError('')
      setTimeout(() => {
        setSuccess(false)
      }, 5000)
    }
  }

  function handleNameChange (event) {
    setName(event.target.value)
    setError('')
  }

  function handleKindChange (event) {
    setKind(event.target.value)
    setError('')
  }

  return (
    <Grid
      container
      justify='center'>
      <Grid item xs={12}><MkHeader textStyle='dark' userInfo={userInfo} /></Grid>
      <Grid item xs={1} />
      <Grid item xs={11} sm={8}>
        <Typography variant='h4' component='h1' gutterBottom>
          Dodaj nowy typ atrakcji
        </Typography>
      </Grid>
      <Grid item xs={11} md={6}>
        <TextField
          label='Nazwa'
          value={name}
          onChange={handleNameChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          helperText='Nazwa widoczna dla użytkowników'
        />
      </Grid>
      <Grid item xs={12} />
      <Grid item xs={11} md={6}>
        <TextField
          label='Typ'
          value={kind}
          onChange={handleKindChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          helperText='Unikatowy identyfikator danego typu atrakcji'
        />
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'
        className={classes.errorMessageContainer}>
        <Grow in={error.length > 0}>
          <Typography className={classes.errorMessage} variant='subtitle2'>
            { error }
          </Typography>
        </Grow>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'
        className={classes.successMessageContainer}>
        <Grow in={success}>
          <Typography className={classes.successMessage} variant='subtitle2'>
            Typ atrakcji został dodany
          </Typography>
        </Grow>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'>
        <Fab
          variant='extended'
          aria-label='create'
          color='primary'
          disabled={name.length < 3 || kind.length < 3 || error.length > 0}
          className={classes.fab}
          onClick={handleCreate}>
          <AddIcon className={classes.extendedIcon} />
          Dodaj
        </Fab>
      </Grid>
    </Grid>
  )
}

AddAttractionType.getInitialProps = async ({ req }) => {
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  return { userInfo }
}
