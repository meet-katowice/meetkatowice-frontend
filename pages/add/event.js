import React, { useState, memo } from 'react'
import cookie from 'cookie'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import TextField from '@material-ui/core/TextField'
import Grow from '@material-ui/core/Grow'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Checkbox from '@material-ui/core/Checkbox'
import AddIcon from '@material-ui/icons/Add'
import IconButton from '@material-ui/core/IconButton'
import ShowIcon from '@material-ui/icons/OpenInNew'
import DateFnsUtils from '@date-io/date-fns'
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from '@material-ui/pickers'
import MkHeader from '../../src/MkHeader'
import { attraction as AttractionApi, event as EventApi, getMe } from '../../src/api'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '90%'
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  fab: {
    margin: `${theme.spacing(1)} auto`
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },
  errorMessageContainer: {
    margin: theme.spacing(4)
  },
  errorMessage: {
    color: 'red'
  },
  successMessageContainer: {
    margin: theme.spacing(4)
  },
  successMessage: {
    color: 'green'
  },
  select: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '100%',
    padding: theme.spacing(1)
  },
  selectFormHelperText: {
    float: 'left'
  },
  attractionsList: {
    maxHeight: '75vh',
    overflowY: 'scroll'
  },
  listElementTitle: {
    margin: theme.spacing(4, 0, 2)
  }
}))

export default function AddEvent (props) {
  const classes = useStyles()

  const [eventToAdd, setEvent] = useState(props.initial.event)
  const [attractionsIds, setAttractionsIds] = useState([])
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)
  async function handleCreate (event) {
    const toAdd = {
      ...eventToAdd,
      attractionIds: attractionsIds
    }
    const result = await EventApi.add(toAdd)

    if (result.error) {
      setSuccess(false)
      setError(result.message)
    } else {
      setSuccess(true)
      setEvent(props.initial.event)
      setError('')
      setTimeout(() => {
        setSuccess(false)
      }, 5000)
    }
  }

  function handleNameChange (event) {
    setEvent({ ...eventToAdd, name: event.target.value })
    setError('')
  }

  function handleDescriptionChange (event) {
    setEvent({ ...eventToAdd, description: event.target.value })
    setError('')
  }

  function handleCheckboxToggle (attractionId) {
    const selected = attractionsIds.includes(attractionId)
    if (selected) {
      setAttractionsIds(attractionsIds.filter((aId) => aId !== attractionId))
    } else {
      setAttractionsIds([...attractionsIds, attractionId])
    }
    setError('')
  }

  function handleDateChange (startDate) {
    setEvent({
      ...eventToAdd,
      startDate: startDate.toISOString()
    })
    setError('')
  }

  function validateEvent (event) {
    return (event.name.length > 3 &&
    event.description.length > 48 &&
    attractionsIds.length > 0)
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid
        container
        justify='center'>
        <Grid item xs={12}><MkHeader textStyle='dark' userInfo={props.userInfo} /></Grid>
        <Grid item xs={1} />
        <Grid item xs={11} sm={8}>
          <Typography variant='h4' component='h1' gutterBottom>
          Dodaj nowe wydarzenie
          </Typography>
        </Grid>
        <Grid item xs={11} md={6}>
          <TextField
            label='Nazwa'
            value={eventToAdd.name}
            onChange={handleNameChange}
            margin='normal'
            className={classes.textField}
            variant='outlined'
            helperText='Nazwa wydarzenia'
          />
        </Grid>
        <Grid item xs={12} />
        <Grid item xs={11} md={6}>
          <TextField
            label='Opis'
            value={eventToAdd.description}
            onChange={handleDescriptionChange}
            margin='normal'
            className={classes.textField}
            variant='outlined'
            multiline
            helperText={`Historia, ciekawostki i inne fakty dotyczące wydarzenia. 48/${eventToAdd.description.length}/4096`}
          />
        </Grid>
        <Grid item xs={12} />
        <Grid item xs={11} md={6}>
          <Typography variant='body1'>
          Wybierz atrakcje:
          </Typography>
          <AttractionsListMemo
            attractions={props.attractions}
            selectedAttractionIds={[...attractionsIds]}
            onCheckboxToggle={handleCheckboxToggle}
          />
        </Grid>
        <Grid item xs={12} />
        <Grid item container xs={12} md={6}
          justify='space-around'>
          <Typography variant='body1'>
            Data rozpoczęcia:
          </Typography>
          <KeyboardDatePicker
            format='dd.MM.yyyy'
            margin='normal'
            value={new Date(Date.parse(eventToAdd.startDate))}
            onChange={handleDateChange}
            disablePast
            disableToolbar
            inputVariant='outlined' />
          <KeyboardTimePicker
            format='HH:mm'
            margin='normal'
            value={new Date(Date.parse(eventToAdd.startDate))}
            onChange={handleDateChange}
            disablePast
            ampm={false}
            inputVariant='outlined' />
        </Grid>
        <Grid item container xs={12}
          alignItems='center'
          alignContent='center'
          justify='center'
          className={classes.errorMessageContainer}>
          <Grow in={error.length > 0}>
            <Typography className={classes.errorMessage} variant='subtitle2'>
              { error }
            </Typography>
          </Grow>
        </Grid>
        <Grid item container xs={12}
          alignItems='center'
          alignContent='center'
          justify='center'
          className={classes.successMessageContainer}>
          <Grow in={success}>
            <Typography className={classes.successMessage} variant='subtitle2'>
            Wydarzenie zostało dodane
            </Typography>
          </Grow>
        </Grid>
        <Grid item container xs={12}
          alignItems='center'
          alignContent='center'
          justify='center'>
          <Fab
            variant='extended'
            aria-label='create'
            color='primary'
            disabled={!validateEvent(eventToAdd)}
            className={classes.fab}
            onClick={handleCreate}>
            <AddIcon className={classes.extendedIcon} />
          Dodaj
          </Fab>
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  )
}

function AttractionsList ({ attractions, selectedAttractionIds, onCheckboxToggle }) {
  return (
    <List>
      {
        attractions.map((attraction, index) => (
          <AttractionListItemMemo
            key={attraction._id}
            selectedAttractionIds={selectedAttractionIds}
            attraction={attraction}
            index={index}
            onCheckboxToggle={onCheckboxToggle} />
        ))
      }
    </List>
  )
}
const AttractionsListMemo = memo(AttractionsList, (prevProps, nextProps) => prevProps.selectedAttractionIds.length === nextProps.selectedAttractionIds.length)

function AttractionListItem ({ selectedAttractionIds, onCheckboxToggle, attraction, index }) {
  const classes = useStyles()

  return (
    <ListItem
      key={attraction._id}
      className={classes.attractionsList}
      button
      dense>
      <ListItemIcon>
        <Checkbox
          edge='start'
          checked={selectedAttractionIds.includes(attraction._id)}
          disableRipple
          onClick={() => onCheckboxToggle(attraction._id)}
          tabIndex={100 + index}
        />
      </ListItemIcon>
      <ListItemText
        primary={attraction.name}
        secondary={`${attraction.kind.name} - ${attraction.description.substring(0, 40)}`}
      />
      <ListItemSecondaryAction>
        <IconButton
          edge='end'
          aria-label='show attraction details'
          onClick={() => window.open(`https://${window.location.host}/attraction/${attraction._id}`)}>
          <ShowIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  )
}
const AttractionListItemMemo = memo(AttractionListItem)

AddEvent.getInitialProps = async ({ req }) => {
  const attractions = await AttractionApi.getAll()
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  return {
    initial: {
      event: {
        name: '',
        description: '',
        attractionIds: [],
        startDate: new Date().toISOString()
      }
    },
    attractions,
    userInfo
  }
}
