import React from 'react'
import cookie from 'cookie'
import Grid from '@material-ui/core/Grid'
import Header from '../../src/MkHeader'
import List from '@material-ui/core/List'
import EventListItem from '../../src/events/EventListItem'
import { attraction as AttractionApi, getMe, comment as CommentsApi } from '../../src/api'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/styles'
import ImageCarousel from '../../src/attractions/details/Carousel'
import ShowOnMap from '../../src/attractions/details/ShowOnMap'
import { Paper, Chip, IconButton } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import Comments from '../../src/comments/Comments'
import { useRouter } from 'next/router'

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2)
  },
  descriptionContainer: {
    padding: theme.spacing(3)
  },
  chipSpacing: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2)
  }
}))

export default function AttractionDetails ({ attraction, comments, userInfo, canEdit, canDelete }) {
  const classes = useStyles()
  const added = new Date(Date.parse(attraction.added))

  const router = useRouter()

  function handleEdit () {
    router.push(`/update/attraction/${attraction._id}`)
  }

  async function handleDelete () {
    if (!confirm('Atrakcja zostanie usunięta z systemu. Kontynuować?')) {
      return
    }
    await AttractionApi.delete(attraction._id)
    router.push('/attractions')
  }

  return (
    <Grid container
      justify='center'>
      <Grid item xs={12}>
        <Header textStyle='dark' userInfo={userInfo} />
      </Grid>
      <Grid className={classes.container} item xs={12} lg={8}>
        <Typography variant='h3'>
          { attraction.name }
        </Typography>
        {
          canEdit && (
            <IconButton onClick={handleEdit} title='Edytuj atrakcję'>
              <EditIcon />
            </IconButton>
          )
        }
        {
          canDelete && (
            <IconButton onClick={handleDelete} title='Usuń atrakcję'>
              <DeleteIcon />
            </IconButton>
          )
        }
      </Grid>
      <Grid className={classes.container} container item xs={12} md={8} lg={5}>
        <Grid item xs={12}>
          <ImageCarousel urls={attraction.images} />
        </Grid>
        <Grid item xs={12}>
          <Chip 
            className={classes.chipSpacing} 
            label={`Dodano dnia: ${added.toLocaleDateString()}`} 
          />
          <Chip 
            className={classes.chipSpacing} 
            label={`Dodano przez: ${attraction.author.username}`} 
          />
          <Chip 
            className={classes.chipSpacing} 
            label={`Typ: ${attraction.kind.name}`} 
          />
        </Grid>
        <Grid item xs={12}>
          <ShowOnMap 
            lat={attraction.location.lat} 
            lng={attraction.location.lng} 
            zoom={16}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} md={4} lg={4}>
        <Paper className={classes.descriptionContainer}>
          <Typography variant='h4'>Opis</Typography>
          <Typography><HtmlDescription content={attraction.description}/></Typography>
        </Paper>
      </Grid>
      <Grid item xs={12} md={8}>
        <Paper className={classes.descriptionContainer} style={{ marginTop: '1em' }}>
          <Typography variant='h4'>Komentarze</Typography>
          <Comments
            comments={comments}
            userInfo={userInfo}
            resource={'attraction'}
            resourceId={attraction._id}
          />
        </Paper>
      </Grid>
    </Grid>
  )
}

function HtmlDescription({ content }) {
  return (<p dangerouslySetInnerHTML={{__html:content}}/>)
}

AttractionDetails.getInitialProps = async ({ req, query }) => {
  const attraction = await AttractionApi.getById(query.id)
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  const comments = await CommentsApi.getAllForResource('attraction', attraction._id)

  function canEdit(userInfo, attraction) {
    if (!userInfo.user || !attraction._id) {
      return false
    }
    
    return (userInfo.user._id.toString() === attraction.authorId.toString()) || (
      userInfo.user.permissions.attractions && userInfo.user.permissions.attractions.includes('U') 
    )
  }

  function canDelete(userInfo, attraction) {
    if (!userInfo.user || !attraction._id) {
      return false
    }
    
    return (userInfo.user._id.toString() === attraction.authorId.toString()) || (
      userInfo.user.permissions.attractions && userInfo.user.permissions.attractions.includes('D') 
    )
  }

  return { 
    attraction, 
    userInfo, 
    comments, 
    canEdit: canEdit(userInfo, attraction), 
    canDelete: canDelete(userInfo, attraction) 
  }
}
