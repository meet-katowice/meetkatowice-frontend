import React, { useState } from 'react'
import cookie from 'cookie'
import Grid from '@material-ui/core/Grid'
import Header from '../../src/MkHeader'
import List from '@material-ui/core/List'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import EventListItem from '../../src/events/EventListItem'
import Participate from '../../src/events/details/Participate'
import { event as EventApi, comment as CommentsApi, getMe } from '../../src/api'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/styles'
import ImageCarousel from '../../src/events/details/Carousel'
import { Paper, Chip } from '@material-ui/core'
import AttractionList from '../../src/events/details/AttractionList'
import { useRouter } from 'next/router'
import Comments from '../../src/comments/Comments'

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(2)
  },
  descriptionContainer: {
    padding: theme.spacing(3),
    marginBottom: theme.spacing(1)
  },
  chipSpacing: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2)
  }
}))

function isParticipating (userInfo, participants) {
  const isParticipating = userInfo && userInfo.user && participants.some(({ _id }) => _id === userInfo.user._id)
  return isParticipating
}

export default function EventDetails ({ event, comments, userInfo, isParticipating, canEdit, canDelete }) {
  const classes = useStyles()
  const startDate = new Date(Date.parse(event.startDate))

  function getImages (event) {
    const images = []
    event.attractions.forEach((attraction) => {
      if (attraction.images.length > 0) {
        images.push([...attraction.images])
      }
    })

    return images;
  }

  const [joined, setJoined] = useState(isParticipating)
  async function joinEvent () {
    await EventApi.join(event._id)
    setJoined(true)
  }

  async function leaveEvent () {
    await EventApi.leave(event._id)
    setJoined(false)
  }

  const router = useRouter()
  function handleParticipationClick () {
    if (!userInfo.user) {
      router.push('/login')
      return
    }

    if (!joined) {
      joinEvent()
    } else {
      leaveEvent()
    }
  }

  function handleEdit () {
    router.push(`/update/event/${event._id}`)
  }

  async function handleDelete () {
    if (!confirm('Wydarzenie zostanie usunięte z systemu. Kontynuować?')) {
      return
    }
    await EventApi.delete(event._id)
    router.push('/')
  }

  return (
    <Grid container
      justify='center'>
      <Grid item xs={12}>
        <Header textStyle='dark' userInfo={userInfo} />
      </Grid>
      <Grid className={classes.container} item xs={12} lg={8}>
        <Typography variant='h3'>
          { event.name }
        </Typography>
        {
          canEdit && (
            <IconButton onClick={handleEdit} title='Edytuj wydarzenie'>
              <EditIcon />
            </IconButton>
          )
        }
        {
          canDelete && (
            <IconButton onClick={handleDelete} title='Usuń wydarzenie'>
              <DeleteIcon />
            </IconButton>
          )
        }
      </Grid>
      <Grid className={classes.container} container item xs={12} md={8} lg={5}>
        <Grid item xs={12}>
          <ImageCarousel urls={getImages(event)} />
        </Grid>
        <Grid item xs={12}>
          <Chip 
            className={classes.chipSpacing} 
            label={`Rozpoczęcie: ${startDate.toLocaleDateString()} ${startDate.toLocaleTimeString()}`} 
          />
          <Chip 
            className={classes.chipSpacing} 
            label={`Dodano przez: ${event.author.username}`} 
          />
          <Chip 
            className={classes.chipSpacing} 
            label={`Typ: ${event.attractions[0].kind.name}`} 
          />
          <Chip 
            className={classes.chipSpacing} 
            label={`Biorących udział: ${event.participants.length}`} 
          />
        </Grid>
        <Participate 
          isParticipating={joined}
          isLoggedIn={!!userInfo.user}
          onClick={handleParticipationClick}
        />
        <Grid item xs={12}>
          <AttractionList attractions={event.attractions} />
        </Grid>
      </Grid>
      <Grid item xs={12} md={4} lg={4}>
        <Paper className={classes.descriptionContainer}>
          <Typography variant='h4'>Opis</Typography>
          <Typography>{ event.description }</Typography>
        </Paper>
      </Grid>
      <Grid item xs={12} md={8}>
        <Paper className={classes.descriptionContainer} style={{ marginTop: '1em' }}>
          <Typography variant='h4'>Komentarze</Typography>
          <Comments 
            comments={comments}
            userInfo={userInfo}
            resource={'event'}
            resourceId={event._id}
          />
        </Paper>
      </Grid>
    </Grid>
  )
}

EventDetails.getInitialProps = async ({ req, query }) => {
  const event = await EventApi.getById(query.id)
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  const comments = await CommentsApi.getAllForResource('event', event._id)

  function canEdit(userInfo, event) {
    if (!userInfo.user || !event._id) {
      return false
    }
    
    return (userInfo.user._id.toString() === event.authorId.toString()) || (
      userInfo.user.permissions.events && userInfo.user.permissions.events.includes('U') 
    )
  }

  function canDelete(userInfo, event) {
    if (!userInfo.user || !event._id) {
      return false
    }
    
    return (userInfo.user._id.toString() === event.authorId.toString()) || (
      userInfo.user.permissions.events && userInfo.user.permissions.events.includes('D') 
    )
  }

  return { 
    event, 
    userInfo, 
    isParticipating: isParticipating(userInfo, event.participants), 
    comments,
    canEdit: canEdit(userInfo, event),
    canDelete: canDelete(userInfo, event) 
  }
}
