import React, { useState } from 'react'
import cookie from 'cookie'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import TextField from '@material-ui/core/TextField'
import Grow from '@material-ui/core/Grow'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import AddIcon from '@material-ui/icons/Add'
import MkHeader from '../../../src/MkHeader'
import { attraction as AttractionApi, attractionType, getMe } from '../../../src/api'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '90%'
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  fab: {
    margin: `${theme.spacing(1)} auto`
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },
  errorMessageContainer: {
    margin: theme.spacing(4)
  },
  errorMessage: {
    color: 'red'
  },
  successMessageContainer: {
    margin: theme.spacing(4)
  },
  successMessage: {
    color: 'green'
  },
  select: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '100%',
    padding: theme.spacing(1)
  },
  selectFormHelperText: {
    float: 'left'
  }
}))

export default function UpdateAttraction (props) {
  const classes = useStyles()

  const [attraction, setAttraction] = useState(props.initial.attraction)
  const [imagesInputValue, setImagesInputValue] = useState(props.initial.imagesInputValue)
  const [error, setError] = useState('')
  const [success, setSuccess] = useState(false)
  async function handleCreate (event) {
    const result = await AttractionApi.update(attraction._id, attraction)

    if (result.error) {
      setSuccess(false)
      setError(result.message)
    } else {
      setSuccess(true)
      setError('')
      setTimeout(() => {
        setSuccess(false)
        window.location.reload()
      }, 5000)
    }
  }

  function handleNameChange (event) {
    setAttraction({ ...attraction, name: event.target.value })
    setError('')
  }

  function handleDescriptionChange (event) {
    setAttraction({ ...attraction, description: event.target.value })
    setError('')
  }

  function handleImagesChange (event) {
    setImagesInputValue(event.target.value)
    const imagesString = event.target.value
    const images = imagesString.split('\n')
    setAttraction({ ...attraction, images: imagesString.length > 0 ? images : [] })
    setError('')
  }

  function handleLatChange (event) {
    setAttraction({ ...attraction,
      location: {
        ...attraction.location,
        lat: event.target.value
      }
    })
    setError('')
  }

  function handleLngChange (event) {
    setAttraction({ ...attraction,
      location: {
        ...attraction.location,
        lng: event.target.value
      }
    })
    setError('')
  }

  const [attractionType, setAttractionType] = useState(props.attractionTypes[0])
  function handleKindIdChange (event) {
    setAttraction({ ...attraction,
      kindId: event.target.value
    })
    setAttractionType(props.attractionTypes.find((a) => a._id === event.target.value))
    setError('')
  }

  function validateAttraction (attraction) {
    return (attraction.name.length > 3 &&
    attraction.description.length > 48 &&
    attraction.kindId.length > 0)
  }

  return (
    <Grid
      container
      justify='center'>
      <Grid item xs={12}><MkHeader textStyle='dark' userInfo={props.userInfo} /></Grid>
      <Grid item xs={1} />
      <Grid item xs={11} sm={8}>
        <Typography variant='h4' component='h1' gutterBottom>
          Zaktualizuj atrakcję
        </Typography>
      </Grid>
      <Grid item xs={11} md={6}>
        <TextField
          label='Nazwa'
          value={attraction.name}
          onChange={handleNameChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          helperText='Nazwa widoczna dla użytkowników'
        />
      </Grid>
      <Grid item xs={12} />
      <Grid item xs={11} md={6}>
        <TextField
          label='Opis'
          value={attraction.description}
          onChange={handleDescriptionChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          multiline
          helperText={`Historia, ciekawostki i inne fakty dotyczące atrakcji. 48/${attraction.description.length}/4096`}
        />
      </Grid>
      <Grid item xs={12} />
      <Grid item xs={11} md={6}>
        <TextField
          label='Linki do zdjęć'
          value={imagesInputValue}
          onChange={handleImagesChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          multiline
          helperText={`Odnośniki do zdjęć atrakcji. ${attraction.images.length}/10`}
        />
      </Grid>
      <Grid item xs={12} />
      <Grid item xs={11} md={3}>
        <TextField
          label='Lokalizacja - szerokość geograficzna'
          value={attraction.location.lat}
          onChange={handleLatChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          helperText={`Szerokość geograficzna`}
        />
      </Grid>
      <Grid item xs={11} md={3}>
        <TextField
          label='Lokalizacja - długość geograficzna'
          value={attraction.location.lng}
          onChange={handleLngChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          helperText={`Długość geograficzna.`}
        />
      </Grid>
      <Grid item xs={12} />
      <Grid item container xs={11} md={6}>
        <FormControl variant='outlined'
          className={classes.select}>
          <Select
            value={attractionType._id}
            onChange={handleKindIdChange}
            variant='outlined'
            inputProps={{
              id: 'attraction-type'
            }}
          >
            {
              props.attractionTypes.map((attractionType) => (
                <MenuItem
                  key={attractionType._id}
                  value={attractionType._id}
                >
                  {attractionType.name}
                </MenuItem>
              ))
            }
          </Select>
          <FormHelperText>Typ atrakcji</FormHelperText>
        </FormControl>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'
        className={classes.errorMessageContainer}>
        <Grow in={error.length > 0}>
          <Typography className={classes.errorMessage} variant='subtitle2'>
            { error }
          </Typography>
        </Grow>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'
        className={classes.successMessageContainer}>
        <Grow in={success}>
          <Typography className={classes.successMessage} variant='subtitle2'>
            Atrakcja została zaktualizowana
          </Typography>
        </Grow>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'>
        <Fab
          variant='extended'
          aria-label='create'
          color='primary'
          disabled={!validateAttraction(attraction)}
          className={classes.fab}
          onClick={handleCreate}>
          <AddIcon className={classes.extendedIcon} />
          Dodaj
        </Fab>
      </Grid>
    </Grid>
  )
}

UpdateAttraction.getInitialProps = async ({ req, query }) => {
  const res = await AttractionApi.getById(query.id)
  const attractionTypes = await attractionType.getAll()
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  return {
    initial: {
      attraction: {
        _id: res._id,
        name: res.name,
        description: res.description,
        images: res.images,
        location: {
          lat: res.location.lat,
          lng: res.location.lng
        },
        kindId: res.kindId
      },
      imagesInputValue: res.images.join('\n')
    },
    error: res.error,
    imagesInputValue: res.images.join('\n'),
    attractionTypes,
    userInfo
  }
}
