import React, { useState, useEffect } from 'react'
import cookie from 'cookie'
import { useRouter } from 'next/router'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import TextField from '@material-ui/core/TextField'
import Grow from '@material-ui/core/Grow'
import UpdateIcon from '@material-ui/icons/Update'
import MkHeader from '../../../src/MkHeader'
import { attractionType, getMe } from '../../../src/api'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '90%'
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  fab: {
    margin: `${theme.spacing(1)} auto`
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },
  errorMessageContainer: {
    margin: theme.spacing(4)
  },
  errorMessage: {
    color: 'red'
  },
  successMessageContainer: {
    margin: theme.spacing(4)
  },
  successMessage: {
    color: 'green'
  }
}))

export default function UpdateAttractionType (props) {
  const classes = useStyles()

  const [name, setName] = useState(props.initial ? props.initial.name ? props.initial.name : '' : '')
  const [kind, setKind] = useState(props.initial ? props.initial.kind ? props.initial.kind : '' : '')
  const [error, setError] = useState(props.initial ? props.initial.error ? props.initial.error : '' : '')
  const [isLoading, setLoading] = useState(true)
  const [success, setSuccess] = useState(false)

  async function getAttractionType (id) {
    if (!id) {
      return
    }

    const result = await attractionType.getById(id)
    if (result.error) {
      setSuccess(false)
      setError(result.message)
    } else {
      setName(result.name)
      setKind(result.kind)
      setError('')
      setSuccess(false)
    }
  }
  const router = useRouter()
  const { id } = router.query
  useEffect(() => {
    if (!props.initial) {
      getAttractionType(id)
    }
  }, [id])

  async function handleCreate (event) {
    const result = await attractionType.update(
      id, 
      props.initial.name !== name ? name : undefined,
      props.initial.kind !== kind ? kind : undefined
    )

    if (result.error) {
      setSuccess(false)
      setError(result.message)
    } else {
      setSuccess(true)
      setName(result.name)
      setKind(result.kind)
      setError('')
      setTimeout(() => {
        setSuccess(false)
        window.location.reload()
      }, 5000)
    }
  }

  function handleNameChange (event) {
    setName(event.target.value)
    setError('')
  }

  function handleKindChange (event) {
    setKind(event.target.value)
    setError('')
  }

  return (
    <Grid
      container
      justify='center'>
      <Grid item xs={12}><MkHeader textStyle='dark' userInfo={props.userInfo} /></Grid>
      <Grid item xs={1} />
      <Grid item xs={11} sm={8}>
        <Typography variant='h4' component='h1' gutterBottom>
          Uaktualnij typ atrakcji
        </Typography>
      </Grid>
      <Grid item xs={11} md={6}>
        <TextField
          label='Nazwa'
          value={name}
          onChange={handleNameChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          disabled={!!props.initial.error}
          helperText='Nazwa widoczna dla użytkowników'
        />
      </Grid>
      <Grid item xs={12} />
      <Grid item xs={11} md={6}>
        <TextField
          label='Typ'
          value={kind}
          onChange={handleKindChange}
          margin='normal'
          className={classes.textField}
          variant='outlined'
          disabled={!!props.initial.error}
          helperText='Unikatowy identyfikator danego typu atrakcji'
        />
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'
        className={classes.errorMessageContainer}>
        <Grow in={error.length > 0}>
          <Typography className={classes.errorMessage} variant='subtitle2'>
            { error }
          </Typography>
        </Grow>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'
        className={classes.successMessageContainer}>
        <Grow in={success}>
          <Typography className={classes.successMessage} variant='subtitle2'>
            Typ atrakcji został uaktualniony
          </Typography>
        </Grow>
      </Grid>
      <Grid item container xs={12}
        alignItems='center'
        alignContent='center'
        justify='center'>
        <Fab
          variant='extended'
          aria-label='create'
          color='primary'
          disabled={(name.length < 3 || kind.length < 3 || error.length > 0) 
            || (props.initial.name === name && props.initial.kind === kind)}
          className={classes.fab}
          onClick={handleCreate}>
          <UpdateIcon className={classes.extendedIcon} />
          Aktualizuj
        </Fab>
      </Grid>
    </Grid>
  )
}

UpdateAttractionType.getInitialProps = async ({ req, query }) => {
  const res = await attractionType.getById(query.id)
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  return { initial: { name: res.name, kind: res.kind, error: res.error }, userInfo }
}
