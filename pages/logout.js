import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import { logout } from '../src/api'

export default function Logout () {
  const router = useRouter()
  let loggedOut = false
  useEffect(async () => {
    if (!loggedOut) {
      loggedOut = true
      await logout()
      router.back()
    }
  })
  return <></>
}
