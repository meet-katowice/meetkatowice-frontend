import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Fab from '@material-ui/core/Fab'
import MkHeader from '../src/MkHeader'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login'
import { authFacebook, authGoogle } from '../src/api'
import { useRouter } from 'next/router'

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(0, 2, 2, 0)
  },
  divider: {
    height: theme.spacing(2)
  },
  fbLoginButton: {
    ...theme.typography.button,
    background: 'linear-gradient(45deg, #4267B2 30%, #476FBF 90%)',
    border: 0,
    borderRadius: '12px',
    boxShadow: '0 3px 5px 2px rgba(66, 103, 178, .3)',
    color: 'white',
    height: 48,
    padding: '0 30px',
    width: '100%',
    transition: '0.3s ease-in-out',
    '&:hover': {
      boxShadow: '1px 5px 8px 3px rgba(66, 103, 178, .5)',
      cursor: 'pointer'
    }
  },
  googLoginButton: {
    ...theme.typography.button,
    background: 'linear-gradient(45deg, #d64136 30%, #E34439 90%)',
    border: 0,
    borderRadius: '12px',
    boxShadow: '0 3px 5px 2px rgba(214, 65, 54, .3)',
    color: 'white',
    height: 48,
    padding: '0 30px',
    width: '100%',
    transition: '0.3s ease-in-out',
    '&:hover': {
      boxShadow: '1px 5px 8px 3px rgba(214, 65, 54, .5)',
      cursor: 'pointer'
    }
  }
}))

export default function Index () {
  const classes = useStyles()
  const router = useRouter()

  async function facebookCallback (response) {
    const accessToken = response.accessToken
    const facebookId = response.userID
    if (accessToken) {
      await authFacebook(accessToken, facebookId)
      router.back()
    }
  }

  async function googleCallback (response) {
    const accessToken = response.accessToken
    if (accessToken) {
      await authGoogle(accessToken)
      router.back()
    }
  }

  return (
    <Grid
      className={classes.root}
      container
      justify='center'
      style={{ minHeight: '80vh' }}>
      <Grid item xs={12}><MkHeader textStyle='dark' hideUserChip /></Grid>
      <Grid item xs={1} />
      <Grid item xs={11} sm={8}>
        <Typography variant='h4' component='h1' gutterBottom>
          Zaloguj się
        </Typography>
        <Typography variant='subtitle1' gutterBottom>
          by móc komentować i dodawać trasy!
        </Typography>
      </Grid>
      <Grid item xs={1} />
      <Grid item className={classes.divider} xs={12} md={1} />
      <Grid item xs={11} sm={8} md={4} lg={3}>
        <FacebookLogin
          appId='750231972057176'
          fields='id,name'
          callback={facebookCallback}
          render={(renderProps) => (
            <Fab
              variant='extended'
              className={classes.fbLoginButton}
              onClick={renderProps.onClick}
              disabled={!renderProps.isSdkLoaded || renderProps.isDisabled || renderProps.isProcessing}
            >
              Zaloguj się z Facebookiem
            </Fab>
          )} />
      </Grid>
      <Grid item className={classes.divider} xs={12} md={1} />
      <Grid item xs={11} sm={8} md={4} lg={3}>
        <GoogleLogin
          clientId='961344191280-76jo8vgajfaais8kufd331u5bmoor15q.apps.googleusercontent.com'
          fields='id,name'
          onSuccess={googleCallback}
          onFailure={(error) => console.error(error)}
          render={(renderProps) => (
            <Fab
              variant='extended'
              className={classes.googLoginButton}
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
            >
              Zaloguj się z Google
            </Fab>
          )} />
      </Grid>
    </Grid>
  )
}
