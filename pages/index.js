import React from 'react'
import cookie from 'cookie'
import Grid from '@material-ui/core/Grid'
import Header from '../src/MkHeader'
import RecommendedEvent from '../src/events/RecommendedEvent'
import Tabs from '../src/home/Tabs'
import List from '@material-ui/core/List'
import EventListItem from '../src/events/EventListItem'
import { event, getMe } from '../src/api'

export default function Index ({ events, userInfo }) {
  const recommendedEvents = events.slice(0, 2)
  return (
    <Grid container
      justify='center'
      alignItems='center'
      alignContent='flex-start'
      style={{ minHeight: '80vh' }}>
      <Grid item xs={12}>
        <Header textStyle='dark' userInfo={userInfo} />
      </Grid>
      <Grid xs={12}>
        <Tabs page={0} />
      </Grid>
      {
        recommendedEvents.map((event) => (
          <Grid key={event._id} item xs={11} sm={6} md={4} lg={3}>
            <RecommendedEvent eventToDisplay={event} />
          </Grid>
        ))
      }
      <Grid item xs={12} />
      <Grid item xs={11} sm={8} md={6}>
        <List>
          {
            events.slice(2, events.length).map((event) => (
              <EventListItem key={event._id} event={event} />
            ))
          }
        </List>
      </Grid>
    </Grid>
  )
}

Index.getInitialProps = async ({ req }) => {
  const events = await event.getAll()
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  return { events, userInfo }
}
