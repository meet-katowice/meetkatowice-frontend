import React from 'react'
import cookie from 'cookie'
import Grid from '@material-ui/core/Grid'
import Header from '../src/MkHeader'
import Tabs from '../src/home/Tabs'
import List from '@material-ui/core/List'
import AttractionListItem from '../src/attractions/AttractionListItem'
import { attraction, getMe } from '../src/api'

export default function Index ({ attractions, userInfo }) {
  return (
    <Grid container
      justify='center'
      alignItems='center'
      alignContent='flex-start'
      style={{ minHeight: '80vh' }}>
      <Grid item xs={12}>
        <Header textStyle='dark' userInfo={userInfo} />
      </Grid>
      <Grid xs={12}>
        <Tabs page={1} />
      </Grid>
      <Grid item xs={12} />
      <Grid item xs={11} sm={8} md={6}>
        <List>
          {
            attractions.map((a) => (
              <AttractionListItem key={a._id} attraction={a} />
            ))
          }
        </List>
      </Grid>
    </Grid>
  )
}

Index.getInitialProps = async ({ req }) => {
  const attractions = await attraction.getAll(false)
  const { sessionId } = cookie.parse(req ? req.headers['cookie'] ? req.headers['cookie'] : '' : document.cookie)
  const userInfo = await getMe(sessionId)
  return { attractions, userInfo }
}
