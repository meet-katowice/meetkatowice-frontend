#!/bin/bash
set -e

if [[ ! -d "node_modules" ]]; then
    npm install --ignore-scripts --no-bin-links --no-audit
fi

exec "$@"
