import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import Typography from '@material-ui/core/Typography'
import { useRouter } from 'next/router'

const useStyles = makeStyles(theme => ({
  fabContainer: {
    padding: theme.spacing(3)
  },
  fab: {
    marginBottom: theme.spacing(1),
    float: 'right',
    zIndex: 100000
  }
}))

export default function Fabs (props) {
  const classes = useStyles(props)
  const { userInfo } = props
  const router = useRouter()
  function handleAddEventClick () {
    router.push('/add/event')
  }
  function handleAddAttractionClick () {
    router.push('/add/attraction')
  }
  function handleAddAttractionTypeClick () {
    router.push('/add/attractionType')
  }

  const userPermissions = userInfo && userInfo.user ? userInfo.user.permissions : []
  const canAddEvent = userPermissions.events && userPermissions.events.includes('C')
  const canAddAttraction = userPermissions.attractions && userPermissions.attractions.includes('C')
  const canAddAttractionType = userPermissions.attractionTypes && userPermissions.attractionTypes.includes('C')
  const showTitle = canAddEvent || canAddAttraction || canAddAttractionType

  return (
    <Grid
      container
      className={classes.fabContainer}
      alignContent='center'
      alignItems='center'
      justify='center'>
      <Grid
        item
        container
        xs={1}
        alignContent='center'
        alignItems='center'
        justify='center'>
        { showTitle && (
          <Typography variant='caption' style={{ textAlign: 'center' }}>
            Dodaj
          </Typography>
        ) 
        }
      </Grid>
      <Grid
        item
        container
        xs={12}
        alignContent='center'
        alignItems='center'
        justify='center'>
        <ButtonGroup
          color='secondary'
          aria-label='outlined primary button group'>
          { canAddEvent && <Button onClick={handleAddEventClick}>Wydarzenie</Button> }
          { canAddAttraction && <Button onClick={handleAddAttractionClick}>Atrakcję</Button> }
          { canAddAttractionType && <Button onClick={handleAddAttractionTypeClick}>Typ atrakcji</Button> }
        </ButtonGroup>
      </Grid>
    </Grid>
  )
}
