import React from 'react'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Link from 'next/link'

export default function CenteredTabs ({ page }) {
  return (
    <Tabs
      value={page}
      indicatorColor='primary'
      textColor='primary'
      centered
    >
      <Link href='/'>
        <Tab label='Wydarzenia' />
      </Link>
      <Link href='/attractions'>
        <Tab label='Zabytki' />
      </Link>
    </Tabs>
  )
}
