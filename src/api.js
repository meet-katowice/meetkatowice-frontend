import fetch from 'isomorphic-unfetch'
// TODO: make it env
const apiRoot = 'https://meetkatowice.eu'
const serverSideApiRoot = 'https://meetkatowice.eu'

function getApiRoot () {
  if (typeof window === 'undefined') return serverSideApiRoot
  return apiRoot
}

export async function authFacebook (accessToken, facebookId) {
  const result = await fetch(`${getApiRoot()}/api/auth/facebook`, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: `{"access_token": "${accessToken}", "facebookUserId": "${facebookId}"}`
  })
  return result.json()
}

export async function authGoogle (accessToken) {
  const result = await fetch(`${getApiRoot()}/api/auth/google`, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: `{"access_token": "${accessToken}"}`
  })
  return result.json()
}

export async function logout () {
  const result = await fetch(`${getApiRoot()}/api/auth/logout`, {
    method: 'post',
    headers: {
      Accept: 'application/json'
    }
  })
  return result.json()
}

export async function getMe (sessionId) {
  const result = await fetch(`${getApiRoot()}/api/me`, {
    method: 'get',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Cookie: `sessionId=${sessionId}`
    }
  })
  return result.json()
}

export const attractionType = {
  add: async (name, kind) => {
    const result = await fetch(`${getApiRoot()}/api/attractionType/add`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name, kind })
    })
    return result.json()
  },
  getAll: async () => {
    const result = await fetch(`${getApiRoot()}/api/attractionType`)
    return result.json()
  },
  getById: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/attractionType/${id}`)
    return result.json()
  },
  update: async (id, name, kind) => {
    const result = await fetch(`${getApiRoot()}/api/attractionType/update/${id}`, {
      method: 'put',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name, kind })
    })
    return result.json()
  }
}

export const attraction = {
  add: async (attraction) => {
    const result = await fetch(`${getApiRoot()}/api/attraction/add`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...attraction })
    })
    return result.json()
  },
  getAll: async (resolveRelatedData = true) => {
    const result = await fetch(`${getApiRoot()}/api/attraction?resolveRelatedData=${resolveRelatedData}`)
    return result.json()
  },
  getById: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/attraction/${id}`)
    return result.json()
  },
  update: async (id, attraction) => {
    const result = await fetch(`${getApiRoot()}/api/attraction/update/${id}`, {
      method: 'put',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...attraction })
    })
    return result.json()
  },
  delete: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/attraction/${id}`, {
      method: 'delete',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    return result.json()
  }
}

export const event = {
  add: async (event) => {
    const result = await fetch(`${getApiRoot()}/api/event/add`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...event })
    })
    return result.json()
  },
  getAll: async () => {
    const result = await fetch(`${getApiRoot()}/api/event`)
    return result.json()
  },
  getById: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/event/${id}`)
    return result.json()
  },
  update: async (id, event) => {
    const result = await fetch(`${getApiRoot()}/api/event/update/${id}`, {
      method: 'put',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...event })
    })
    return result.json()
  },
  join: async (eventId) => {
    const result = await fetch(`${getApiRoot()}/api/event/${eventId}/join`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ join: true })
    })
    return result.json()
  },
  leave: async (eventId) => {
    const result = await fetch(`${getApiRoot()}/api/event/${eventId}/join`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ join: false })
    })
    return result.json()
  },
  delete: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/event/${id}`, {
      method: 'delete',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    return result.json()
  }
}

export const comment = {
  add: async (comment) => {
    const result = await fetch(`${getApiRoot()}/api/comment/add`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...comment })
    })
    return result.json()
  },
  getAllForResource: async (resource, resourceId) => {
    const result = await fetch(`${getApiRoot()}/api/comment/${resource}/${resourceId}`)
    return result.json()
  },
  getById: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/comment/${id}`)
    return result.json()
  },
  update: async (id, comment) => {
    const result = await fetch(`${getApiRoot()}/api/comment/update/${id}`, {
      method: 'put',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...comment })
    })
    return result.json()
  },
  delete: async (id) => {
    const result = await fetch(`${getApiRoot()}/api/comment/${id}`, {
      method: 'delete',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    return result.json()
  }
}
