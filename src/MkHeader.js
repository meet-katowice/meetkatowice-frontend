import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Chip from '@material-ui/core/Chip'
import Collapse from '@material-ui/core/Collapse'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import FaceIcon from '@material-ui/icons/Face'
import { useRouter } from 'next/router'
import Fabs from './Fabs'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'block',
    margin: theme.spacing(2, 0, 2)
  },
  logo: {
    fontWeight: 'bold',
    marginLeft: theme.spacing(2),
    fontSize: theme.width >= 600 ? '2em' : '1.5em',
    color: (props) => props.textStyle === 'dark' ? '#222' : '#fafafa'
  },
  userChip: {
    float: 'right',
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(1.5),
    fontSize: theme.width >= 600 ? '1em' : '0.8em',
    wordBreak: 'break-word',
    color: (props) => props.textStyle === 'dark' ? '#222' : '#fafafa'
  },
  menu: {
    position: 'fixed',
    right: theme.spacing(2),
    top: theme.spacing(6),
    width: '160px',
    wordBreak: 'break-word',
    color: (props) => props.textStyle === 'dark' ? '#222' : '#fafafa',
    zIndex: 1000
  },
  menuItem: {
    textDecoration: 'none',
    color: (props) => props.textStyle === 'dark' ? '#222' : '#fafafa'
  }
}))

export default function MkHeader (props) {
  const classes = useStyles(props)
  const { userInfo } = props
  const { username, fullname } = userInfo && userInfo.user ? userInfo.user : { username: undefined }
  const router = useRouter()
  function handleUserClick () {
    if (!username) {
      router.push('/login')
    } else {
      setMenuOpen(!menuOpen)
    }
  }

  const [menuOpen, setMenuOpen] = useState(false)
  function handleClickAway () {
    setMenuOpen(false)
  }

  function getMenuEntries () {
    const entries = [{
      href: '/logout',
      name: 'Wyloguj się'
    }]

    // const userPermissions = userInfo && userInfo.user ? userInfo.user.permissions : []

    // if (userPermissions.events && userPermissions.events.includes('C')) {
    //   entries.push({
    //     href: '/add/event',
    //     name: 'Dodaj wydarzenie'
    //   })
    // }

    // if (userPermissions.attractions && userPermissions.attractions.includes('C')) {
    //   entries.push({
    //     href: '/add/attraction',
    //     name: 'Dodaj atrakcję'
    //   })
    // }

    // if (userPermissions.attractionTypes && userPermissions.attractionTypes.includes('C')) {
    //   entries.push({
    //     href: '/add/attractionType',
    //     name: 'Dodaj typ atrakcji'
    //   })
    // }

    return entries
  }

  return (
    <Grid container>
      <Grid item xs={8} md={6}>
        <Link href='/' className={classes.root}>
          <Typography className={classes.logo} variant='h4' color='primary'>
            MeetKatowice.eu
          </Typography>
        </Link>
      </Grid>
      {
        !props.hideUserChip &&

        <ClickAwayListener onClickAway={handleClickAway}>
          <>
            <Grid item xs={4} md={6}>
              <Chip
                icon={<FaceIcon />}
                label={fullname || username || 'Zaloguj się'}
                onClick={handleUserClick}
                className={classes.userChip}
                variant='outlined'
              />
            </Grid>
            <Grid item xs={12} className={classes.menu}>
              <Collapse in={menuOpen} style={{ transformOrigin: '100% 0 0' }}>
                <Paper>
                  {
                    getMenuEntries().map(({ href, name }) => (
                      <Link key={href} href={href}>
                        <MenuItem className={classes.menuItem}>{name}</MenuItem>
                      </Link>
                    ))
                  }
                </Paper>
              </Collapse>
            </Grid>
          </>
        </ClickAwayListener>
      }
      <Fabs userInfo={userInfo} />
    </Grid>
  )
}
