import React from 'react'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Typography from '@material-ui/core/Typography'

export default function Footer () {
  return (
    <Grid container
      justify='center'
      alignItems='center'
      alignContent='flex-start'
      style={{ marginBottom: '2em', marginTop: '1em' }}>
      <Grid item xs={11} md={10} lg={8}>
        <Card
          style={{ padding: '1.2em' }}>
          <Typography variant='h6'>
              Co to jest?
          </Typography>
          <Typography variant='body1'>
            MeetKatowice.eu to platforma mająca na celu ułatwienie zapoznania się z Katowicami.
            Wykorzystując bogactwo zabytków bazy <a href='https://otwartezabytki.pl/' target='_blank'>Otwarte Zabytki</a>,
            każdy użytkownik może stworzyć swoją ścieżkę i wybrać się w edukacyjną podróż.
            MeetKatowice.eu ułatwia tworzenie wspomnianych ścieżek i dzielenie się, a nawet wspólne zwiedzanie
            z innymi użytkownikami.
          </Typography>
          <Typography variant='button'>
            <a href='/'>Zaplanowane wycieczki</a> | <a href='/attractions'>Lista zabytków</a> | <a href='/add/event'>Dodaj trasę</a>
          </Typography>
          <Typography variant='h6'>
              Dla kogo?
          </Typography>
          <Typography variant='body1'>
            MeetKatowice.eu dostępne jest dla każdego, bez żadnych opłat, dzięki danym dostępnym w bazie <a href='https://otwartezabytki.pl/' target='_blank'>Otwarte Zabytki</a>.
            Projekt został zrealizowany w ramach programu <a href='https://mikrogranty.medialabkatowice.eu/' target='_blank'>Mikrogranty Medialabu</a>.
          </Typography>
          <Typography variant='button'>
            <a href='https://medialabkatowice.eu' target='_blank'>Medialab Katowice</a> | <a href='http://miasto-ogrodow.eu/' target='_blank'>Miasto Ogrodów</a> | <a href='http://miasto-ogrodow.eu/strona/rodo/mid/236' target='_blank'>Polityka prywatności</a>
          </Typography>
        </Card>
      </Grid>

    </Grid>
  )
}
