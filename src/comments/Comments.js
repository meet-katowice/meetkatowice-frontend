import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { makeStyles } from '@material-ui/core/styles'
import Grow from '@material-ui/core/Grow'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import Typography from '@material-ui/core/Typography'
import { comment as CommentApi } from '../api'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: 'inline'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  errorMessage: {
    color: 'red',
    textAlign: 'center'
  }
}))

export default function Comments ({ resource, resourceId, userInfo, comments }) {
  const [allComments, setAllComments] = useState(comments)
  function handleNewComment (comment) {
    const commentWithAuthor = { ...comment, author: userInfo.user, authorId: userInfo.user._id }
    setAllComments([...allComments, commentWithAuthor])
  }

  async function handleDeleteComment (commentId) {
    setAllComments(allComments.filter(({ _id }) => _id !== commentId))
    await CommentApi.delete(commentId)
  }

  const canDelete = (userInfo, comment) => {
    if (!userInfo.user || !comment._id) {
      return false
    }

    if (userInfo.user._id.toString() === comment.authorId.toString()) {
      return true
    }

    if (!!userInfo.user.permissions.comments && userInfo.user.permissions.comments.includes('D')) {
      return true
    }

    return false
  }

  return (
    <div>
      { allComments.length > 0
        ? <CommentsList
          onCommentDelete={handleDeleteComment}
          comments={allComments.map((comment) => ({ ...comment, canDelete: canDelete(userInfo, comment) }))}
        />
        : <NoComments />
      }
      { userInfo.user ? <CreateNewComment resource={resource} resourceId={resourceId} onNewComment={handleNewComment} /> : <GoToLogin /> }
    </div>
  )
}

function Comment ({ id, name, date, content, canDelete, onDelete }) {
  const classes = useStyles()
  const dateAdded = new Date(Date.parse(date))

  return (
    <ListItem alignItems='flex-start'>
      <ListItemText
        primary={name}
        secondary={
          <>
            { `${dateAdded.toLocaleDateString()} ` }

            <Typography
              component='span'
              variant='body2'
              className={classes.inline}
              color='textPrimary'
            >
              { content }
            </Typography>
          </>
        }
      />
      {
        canDelete &&
        <ListItemSecondaryAction>
          <IconButton
            edge='end'
            aria-label='delete'
            title='Usuń komentarz'
            onClick={() => onDelete(id)}>
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      }
    </ListItem>
  )
}

function CommentsList ({ comments, onCommentDelete }) {
  return (
    <List>
      {
        comments.map(({ _id, author, added, content, canDelete }) => (
          <Comment
            key={_id}
            id={_id}
            name={author.fullname || author.username}
            date={added}
            onDelete={onCommentDelete}
            canDelete={canDelete}
            content={content} />
        ))
      }
    </List>
  )
}

function NoComments () {
  return (
    <Typography>
      Nikt jeszcze niczego nie napisał.
    </Typography>
  )
}

function GoToLogin () {
  const router = useRouter()
  function handleClick () {
    router.push('/login')
  }

  return (
    <Button onClick={handleClick}>
      Zaloguj się, by móc komentować
    </Button>
  )
}

function CreateNewComment ({ resource, resourceId, onNewComment }) {
  const classes = useStyles()

  const [content, setContent] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  function handleContentChange (event) {
    setContent(event.target.value)
    setErrorMessage('')
  }

  async function handleSend () {
    const addedComment = await CommentApi.add({
      content,
      refersTo: {
        resource,
        resourceId
      }
    })

    if (addedComment._id) {
      setContent('')
      setErrorMessage('')
      onNewComment(addedComment)
    } else {
      setErrorMessage(addedComment.message)
    }
  }

  return (
    <>
      <TextField
        id='new-comment'
        fullWidth
        label='Napisz komentarz'
        className={classes.textField}
        value={content}
        onChange={handleContentChange}
        margin='normal'
        variant='outlined'
      />
      <Grow in={errorMessage.length > 0}>
        <Typography className={classes.errorMessage}>{ errorMessage }</Typography>
      </Grow>
      <Button
        variant='contained'
        fullWidth
        color='secondary'
        disabled={errorMessage.length > 0}
        onClick={handleSend}>
          Wyślij
      </Button>
    </>
  )
}
