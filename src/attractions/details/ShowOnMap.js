import React from 'react'
import { Button } from '@material-ui/core'

export default function ShowOnMap ({ lat = 50.270908, lng = 19.039993, zoom = 15 }) {
  return (
    <a target='_blank' href={`https://google.com/maps/search/?api=1&map_action=pano&query=${lat},${lng}`}>
      <Button variant='text' color='secondary'>
        Pokaż na mapie
      </Button>
    </a>
  )
}
