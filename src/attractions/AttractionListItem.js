import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import Paper from '@material-ui/core/Paper'
import { useRouter } from 'next/router'

const useStyles = makeStyles(theme => ({
  paper: {
    borderRadius: 0
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: 'inline'
  }
}))

export default function AttractionListItem ({ attraction }) {
  const classes = useStyles()
  const router = useRouter()

  function getCoverImageUrl (attraction) {
    return attraction.images[0] || '/static/event/default1.jpg'
  }
  return (
    <>
      <Paper className={classes.paper}>
        <ListItem button alignItems='flex-start'
          onClick={() => router.push(`/attraction/${attraction._id}`)}>
          <ListItemAvatar>
            <Avatar src={getCoverImageUrl(attraction)} />
          </ListItemAvatar>
          <ListItemText
            primary={attraction.name}
            secondary={
              attraction.description.substring(0, 80)
            }
          />
        </ListItem>
      </Paper>
    </>
  )
}
