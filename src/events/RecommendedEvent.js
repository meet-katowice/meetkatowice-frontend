import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Link from 'next/link'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: '100%',
    margin: theme.spacing(1)
  },
  media: {
    height: 200
  }
}))

export default function RecommendedEvent (props) {
  const { eventToDisplay } = props
  function getCoverImageUrl (eventToDisplay) {
    return eventToDisplay.attractions[0].images[0] || '/static/event/default1.jpg'
  }

  const classes = useStyles()
  return (
    <Card className={classes.card}>
      <Link href={`/event/${eventToDisplay._id}`}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={getCoverImageUrl(eventToDisplay)}
          />
          <CardContent>
            <Typography gutterBottom variant='h5' component='h2'>
              { eventToDisplay.name }
            </Typography>
            <Typography variant='body2' color='textSecondary' component='p'>
              { eventToDisplay.description.length > 180 ? `${eventToDisplay.description.slice(0, 177)}...` : eventToDisplay.description }
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
      <CardActions>
        <Link href={`/event/${eventToDisplay._id}`}>
          <Button size='small' color='primary'>
            Zobacz
          </Button>
        </Link>
      </CardActions>
    </Card>
  )
}
