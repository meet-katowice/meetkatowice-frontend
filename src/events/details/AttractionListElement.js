import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import IconButton from '@material-ui/core/IconButton'
import MapIcon from '@material-ui/icons/Map'
import OpenIcon from '@material-ui/icons/OpenInNew'
import Link from 'next/link'

export default function AttractionListItem ({ attraction }) {
  return (
    <ListItem>
      <ListItemText
        primary={attraction.name}
        secondary={attraction.kind.name}
      />
      <ListItemSecondaryAction>
        <a target='_blank' href={`https://google.com/maps/search/?api=1&map_action=pano&query=${attraction.location.lat},${attraction.location.lng}`}>
          <IconButton
            edge='end'
            aria-label='show on a map'
            title='Pokaż na mapie'>
            <MapIcon />
          </IconButton>
        </a>
        <Link href={`/attraction/${attraction._id}`}>
          <IconButton
            edge='end'
            aria-label='delete'
            title='Pokaż szczegóły atrakcji'>
            <OpenIcon />
          </IconButton>
        </Link>
      </ListItemSecondaryAction>
    </ListItem>
  )
}
