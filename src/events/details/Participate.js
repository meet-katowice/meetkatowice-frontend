import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  }
}))

export default function ({ isParticipating, isLoggedIn, onClick }) {
  const classes = useStyles()
  return (
    <Button
      variant='contained'
      fullWidth
      color='primary'
      className={classes.button}
      onClick={onClick}>
      { !isLoggedIn ? 'Zaloguj się, aby móc dołączać do wydarzeń' : isParticipating ? 'Opuść' : 'Dołącz' }
    </Button>
  )
}
