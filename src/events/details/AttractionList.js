import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import AttractionListItem from './AttractionListElement'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper
  }
}))

export default function AttractionList ({ attractions }) {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Typography variant='h6'>Trasa</Typography>
      <Paper>
        <List>
          {
            attractions.map((attraction) => (
              <AttractionListItem key={attraction._id} attraction={attraction} />
            ))
          }
        </List>
      </Paper>
    </div>
  )
}
