import React from 'react'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import { Carousel } from 'react-responsive-carousel'

export default function ImageCarousel ({ urls }) {
  return (
    <Carousel>
      {
        urls.map((url, index) => (
          <div key={`${url}-${index}`}>
            <img src={url} />
          </div>
        ))
      }
    </Carousel>
  )
}
