import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Divider from '@material-ui/core/Divider'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import { useRouter } from 'next/router'

const useStyles = makeStyles(theme => ({
  paper: {
    borderRadius: 0
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: 'inline'
  }
}))

export default function EventListItem ({ event }) {
  const classes = useStyles()
  const router = useRouter()

  function getCoverImageUrl (eventToDisplay) {
    return eventToDisplay.attractions[0].images[0] || '/static/event/default1.jpg'
  }
  return (
    <>
      <Paper className={classes.paper}>
        <ListItem button alignItems='flex-start'
          onClick={() => router.push(`/event/${event._id}`)}>
          <ListItemAvatar>
            <Avatar src={getCoverImageUrl(event)} />
          </ListItemAvatar>
          <ListItemText
            primary={event.name}
            secondary={
              <>
                <Typography
                  component='span'
                  variant='body2'
                  className={classes.inline}
                  color='textPrimary'
                >
                  { new Date(Date.parse(event.startDate)).toLocaleDateString() }
                </Typography>
                { ` ${event.description.substring(0, 80)}` }
              </>
            }
          />
        </ListItem>
      </Paper>
    </>
  )
}
