FROM node:12

ENV TZ Europe/Warsaw

COPY ./package.json /opt/meetkatowice-frontend/
COPY ./package-lock.json /opt/meetkatowice-frontend/
WORKDIR /opt/meetkatowice-frontend

RUN npm install

ADD . /opt/meetkatowice-frontend

COPY docker-entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 3000

CMD [ "npm", "start" ]
